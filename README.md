# first vue axios with json server

## Project setup
```
npm install
```

### Install Global JSON Server 
```
npm install -g json-server
```

### Compiles and hot-reloads for development
```
npm run serve
```
### Start JSON Server
```
npm run db
```
### Compiles and hot-reloads for development and Start JSON Server Parallel
```
npm run test
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
